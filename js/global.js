/**
 * @file
 * Global utilities.
 *
 */
(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.yg_corporate_lite = {
    attach: function (context, settings) {

      $(function () {
        $(".home-slider-1").owlCarousel({
          autoplay: true,
          autoplayTimeout: 5000,
          loop: true,
          margin: 24,
          dots:true,
          responsive: {
              0:{
                  items:1
              },
              769:{
                  items:1
              },
              1000:{
                  items:1
              }
          }
        });
        var owl1 = $(".home-slider-1");
        owl1.owlCarousel();
        $(once('homeSliderCarousel','.home-slider .arrows .next')).click(function () {
          owl1.trigger("next.owl.carousel");
        });
          $(once('homeSliderCarousel',".home-slider .arrows .prev")).click(function () {
          owl1.trigger("prev.owl.carousel");
        });

      });

      $(function () {
        $(".portfolio-slider").owlCarousel({
          autoplay: true,
          autoplayTimeout: 5000,
          loop: true,
          margin:0,
          dots:false,
          responsive: {
              0:{
                  items:1
              },
              769:{
                  items:1
              },
              1000:{
                  items:1
              }
          }
        });
        
        var owl1 = $(".portfolio-slider");
        owl1.owlCarousel();
        $(once('homeSliderCarousel','.portfolio .arrows .next')).click(function () {
          owl1.trigger("next.owl.carousel");
        });
          $(once('homeSliderCarousel',".portfolio .arrows .prev")).click(function () {
          owl1.trigger("prev.owl.carousel");
        });

      });
      
      AOS.init();
      
    }
  };

})(jQuery ,Drupal);
