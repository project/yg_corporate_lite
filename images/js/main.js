(function($, Drupal) {
    'use strict';

    function main() {
        (function() {
            'use strict';
            $('a.page-scroll, #menu li:first-child a').click(function() {
                if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    if (target.length) {
                        $('html,body').animate({
                            scrollTop: target.offset().top - 50
                        }, 900);
                        return false;
                    }
                }
            });
            $("#menu li:first-child a").addClass("page-scroll");
            $("#menu li:first-child a").attr("href", "#home");
            $("#menu").addClass("navbar-right");
            $('body').scrollspy({
                target: '.navbar-default',
                offset: 80
            });
            var href = document.location.href;
            var hostn = document.location.hostname;
            var PathSegment = href.substr(href.lastIndexOf('/') + 1);
            if (!$("body").hasClass("path-frontpage")) {
                $("#menu li:nth-child(1)").removeClass("active");
                $("#menu li:nth-child(6)").addClass("active");
                $("#menu li a").each(function() {
                    var $this = $(this);
                    var _href = $this.attr("href");
                    $this.attr("href", "/" + _href);
                });
            }
        }());
    }
    main();
    $(window).scroll(function() {
        if ($("body").offset().top > 50) {
            $(".navbar-collapse").addClass("in");
        } else {
            $(".navbar-collapse").removeClass("in");
        }
    });
})(jQuery, Drupal);